#!/usr/bin/env python3
# log2csv.py -*-python-*-
#
# Copyright 2019 by Luke Meyers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

import argparse
import csv
import sys
import time

class Converter(object):
  def __init__(self, input, output):
    self.input = input
    self.output = output

  def convert(self):
    with open(self.input, 'r') as fin:
      with open(self.output, 'w') as fout:
        writer = csv.DictWriter(fout, fieldnames=['Time', 'EpochTime',
                                                  'Heartrate', 'SpO2', 'GSR',
                                                  'Systolic', 'Diastolic',
                                                  'CuffRate'])
        writer.writeheader()
        prevtime = None
        heartrate = 0
        spo2 = 0
        gsr = 0
        systolic = 0
        diastolic = 0
        cuffrate = 0
        for line in fin:
          items = line.split()
          t = int(items[0])
          if prevtime is None:
            prevtime = t
          if t != prevtime:
            writer.writerow({'Time': time.strftime('%x %X',
                                                   time.localtime(prevtime)),
                             'EpochTime': prevtime,
                             'Heartrate': heartrate,
                             'SpO2': spo2,
                             'GSR': gsr,
                             'Systolic': systolic,
                             'Diastolic': diastolic,
                             'CuffRate' : cuffrate})
            prevtime = t
            heartrate = 0
            spo2 = 0
            gsr = 0
            systolic = 0
            diastolic = 0
            cuffrate = 0
          if items[1] == 'pulse':
            heartrate = int(items[2])
            spo2 = int(items[4])
          if items[1] == 'gsr':
            gsr = float(items[2])
          if items[1] == 'sys':
            systolic = int(items[2])
            diastolic = int(items[4])
            cuffrate = int(items[6])

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='log2csv')
  parser.add_argument('--input', '-i', type=str, default=None,
                      help='Input file in meditation-monitor.py log format')
  parser.add_argument('--output', '-o', type=str, default=None,
                      help='Output file in CSV format')
  args = parser.parse_args()

  if not args.input or not args.output:
    parser.print_help()
    sys.exit(-1)

  converter = Converter(args.input, args.output)
  converter.convert()
  sys.exit(0)
