#!/bin/bash
# gnuplot.sh -*-ksh-*-

gnuplot <<"EOF"
set output "lauren-1a.png"
set terminal pngcairo size 1200,900
set grid
#set title "Subject L: Nadi Shodana Pranayama\n(10s inhale, 30s hold, 20s exhale)"
set xlabel "Minutes Since Start of Meditation\n"
set ylabel "Heart Rate (bpm)\nSpO2 (%)"
set yrange [50:100]
set y2range [0.5:3]
set y2label "GSR"
set y2tics
set datafile separator ","
set datafile missing "0"
set key horiz outside bottom center box

set arrow from 2,50 to 2,100 nohead lc rgb '#808080'
set label "warm-up ends" at 2,50.5 rotate by 90 offset 1,1

set arrow from 20,50 to 20,100 nohead lc rgb '#808080'
set label "heat + rigidity" at 20,50.5 rotate by 90 offset 1,1

plot "lauren-20190726-slow.csv" using ($2-1564183849)/60:3 with lines lt -1 t "Heart Rate", \
     "lauren-20190726-slow.csv" using ($2-1564183849)/60:4 with lines lt -1 dt 3 t "SpO2", \
     "lauren-20190726-slow.csv" using ($2-1564183849)/60:5 with lines lt -1 dt 2 axis x1y2 t "GSR"
EOF
convert lauren-1a.png MEYERS0002.tiff

gnuplot <<"EOF"
set output "lauren-1b.png"
set terminal pngcairo size 1200,900
set grid
#set title "Subject L: Nadi Shodana Pranayama\n(10s inhale, 30s hold, 20s exhale)"
set xlabel "Minutes Since Start of Meditation\n"
set ylabel "Blood Pressure (mmHg)"
set yrange [50:150]
set datafile separator ","
set datafile missing "0"
set key horiz outside bottom center box

set arrow from 2,50 to 2,150 nohead lc rgb '#808080'
set label "warm-up ends" at 2,110 rotate by 90 offset 1,1

set arrow from 20,50 to 20,150 nohead lc rgb '#808080'
set label "heat+rigidity starts" at 20,110 rotate by 90 offset 1,1

plot "lauren-20190726-slow.csv" using ($2-1564183849)/60:6 with points pt 8 ps 3 lc rgb "black" t "Systolic", \
     "lauren-20190726-slow.csv" using ($2-1564183849)/60:7 with points pt 19 ps 3 lc rgb "black" t "Diastolic"
EOF
convert lauren-1b.png MEYERS0003.tiff

gnuplot <<"EOF"
set output "lauren-2.png"
set terminal pngcairo size 1200,900
set grid
#set title "Subject L: Kapalbhati (rapid) Breath\n(SpO2 constant at 99%)"
set xlabel "Minutes Since Start of Meditation\n"
set ylabel "Heart Rate (bpm)\nBlood Pressure (mmHg)"
set yrange [50:120]
set y2range [1:2.4]
set y2label "GSR"
set y2tics
set datafile separator ","
set datafile missing "0"
set key horiz outside bottom center box

plot "lauren-20190726-fast.csv" using ($2-1564183287)/60:3 with lines lt -1 t "Heart Rate", \
     "lauren-20190726-fast.csv" using ($2-1564183287)/60:5 with lines lt -1 dt 2 axis x1y2 t "GSR", \
     "lauren-20190726-fast.csv" using ($2-1564183287)/60:6 with points pt 8 ps 3 lc rgb "black" t "Systolic", \
     "lauren-20190726-fast.csv" using ($2-1564183287)/60:7 with points pt 19 ps 3 lc rgb "black" t "Diastolic"
EOF
convert lauren-2.png MEYERS0001.tiff

gnuplot <<"EOF"
set output "austin.png"
set terminal pngcairo size 1200,900
set grid
#set title "Subject A: Pranayama\n(10s inhale, 10s kumbhaka, 20s exhale; fasting 16 hours)"
set xlabel "Minutes Since Start of Meditation\n"
set ylabel "Heart Rate (bpm)\nSpO2 (%)"
set yrange [60:120]
set y2range [1:4]
set y2label "GSR"
set y2tics
set datafile separator ","
set datafile missing "0"
set key horiz outside bottom center box

plot "austin-20190804.csv" using ($2-1564936397)/60:3 with lines lt -1 t "Heart Rate", \
     "austin-20190804.csv" using ($2-1564936397)/60:4 with lines lt -1 dt 3 t "SpO2", \
     "austin-20190804.csv" using ($2-1564936397)/60:5 with lines lt -1 dt 2 axis x1y2 t "GSR"
EOF
convert austin.png MEYERS0004.tiff

gnuplot <<"EOF"
set output "daniel.png"
set terminal pngcairo size 1200,900
set grid
#set title "Subject D: Pranayama\n(5s inhale, pause, 10s exhale)"
set xlabel "Minutes Since Start of Meditation\n"
set ylabel "Heart Rate (bpm)\nSpO2 (%)\nBlood Pressure (mmHg)"
set yrange [70:150]
set y2range [1.0:2.6]
set y2label "GSR"
set y2tics
set datafile separator ","
set datafile missing "0"
set key horiz outside bottom center box

plot "daniel-201908004.csv" using ($2-1564934612)/60:3 with lines lt -1 t "Heart Rate", \
     "daniel-201908004.csv" using ($2-1564934612)/60:4 with lines lt -1 dt 3 t "SpO2", \
     "daniel-201908004.csv" using ($2-1564934612)/60:5 with lines lt -1 dt 2 axis x1y2 t "GSR", \
     "daniel-201908004.csv" using ($2-1564934612)/60:6 with points pt 8 ps 3 lc rgb "black" t "Systolic", \
     "daniel-201908004.csv" using ($2-1564934612)/60:7 with points pt 19 ps 3 lc rgb "black" t "Diastolic"
EOF
convert daniel.png MEYERS0005.tiff

xv MEYERS*.tiff
