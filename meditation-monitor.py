#!/usr/bin/env python3
# meditation-monitor.py -*-python-*-
# Copyright 2019 by Luke Meyers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

# Standard python3 modules
import argparse
import binascii
import json
import multiprocessing
import os
import re
import select
import struct
import sys
import time

# Non-standard modules
try:
  import serial
except:
  print('Cannot import "serial". Please sudo apt-get install python3-serial')
  sys.exit(1)

class USB(object):
  SYSPATH = '/sys/bus/usb/devices'

  def __init__(self, verbose=False):
    self.verbose = verbose
    self.usb_devices = self.get_usb_devices()

  def _readfile(self, path):
    try:
      with open(path, 'r') as fp:
        return fp.read().strip()
    except:
      return ''

  def _find_devices(self, dirname):
    devices = set()
    with os.scandir(dirname) as it:
      for entry in it:
        if entry.is_dir() and not entry.is_symlink():
          devices |= self._find_devices(os.path.join(dirname, entry.name))
        if re.search('tty.*[0-9]', entry.name):
          devices.add(entry.name)
        if re.search('hidraw[0-9]', entry.name):
          devices.add(entry.name)
    return devices

  def _get_usb_device(self, dirname):
    vendorid = self._readfile(os.path.join(dirname, 'idVendor'))
    if vendorid == '':
      return None
    vendorid = int(vendorid, 16)
    productid = self._readfile(os.path.join(dirname, 'idProduct'))
    productid = int(productid, 16)
    vendor_name = self._readfile(os.path.join(dirname, 'manufacturer'))
    product_name = self._readfile(os.path.join(dirname, 'product'))
    busnum = int(self._readfile(os.path.join(dirname, 'busnum')))
    devnum = int(self._readfile(os.path.join(dirname, 'devnum')))
    devices = sorted(self._find_devices(dirname))
    serial = self._readfile(os.path.join(dirname, 'serial'))
    return dirname, busnum, devnum, vendorid, productid, vendor_name, \
      product_name, devices, serial

  def get_usb_devices(self):
    usb_devices = []
    with os.scandir(USB.SYSPATH) as it:
      for entry in it:
        if entry.is_dir():
          device = self._get_usb_device(os.path.join(USB.SYSPATH, entry.name))
          if device is not None:
            usb_devices.append(device)
    return usb_devices

  def list(self, use_json=False):
    results = []
    for path, busnum, devnum, vendorid, productid, vendor_name, product_name, \
        devices, serial in sorted(self.usb_devices,
                                  key=lambda x: x[1] * 1000 + x[2]):
      d = dict()
      d['path'] = path
      d['busnum'] = busnum
      d['devnum'] = devnum
      d['vendorid'] = vendorid
      d['productid'] = productid
      d['vendor_name'] = vendor_name
      d['product_name'] = product_name
      d['devices'] = devices
      d['serial'] = serial
      results.append(d)
      if not use_json and self.verbose:
        print('Bus %03d Dev %03d %04x:%04x %s %s %s' % (
          busnum,
          devnum,
          vendorid,
          productid,
          product_name if product_name != '' else '???',
          list(devices), '' if serial == '' else 'sn=' + serial))
    if use_json:
      print(json.dumps(results, indent=4))
    return results

class CMS50DPlus(object):
  def __init__(self, path, queue, verbose=False):
    self.path = path
    self.queue = queue
    self.verbose = verbose

  def read(self):
    s = serial.Serial(self.path, 115200)
    s.bytesize = serial.EIGHTBITS
    s.parity = serial.PARITY_NONE
    s.stopbits = serial.STOPBITS_ONE
    s.timeout = 2
    s.xonoff = True
    s.rtscts = False
    s.dsrdrt = False
    s.write(b'\x7d\x81\xa2\x80\x80\x80\x80\x80\x80') # Stop sending
    s.write(b'\x7d\x81\xa1\x80\x80\x80\x80\x80\x80') # Start sending
    prevtime = 0
    while True:
      byte = s.read(1)
      if len(byte) != 1:
        if self.verbose:
          print("# wakeup", time.ctime())
        time.sleep(1)
        s.write(b'\x7d\x81\xa1\x80\x80\x80\x80\x80\x80') # Start sending
        continue
      if byte != b'\x01':
        continue
      data = s.read(8)
      pulse = data[4] - 128
      SpO2 = data[5] - 128
      if self.verbose and False:
        signalStrength = data[1] & 0x0f
        searching = bool(data[1] & 0x10)
        lowSpO2 = bool(data[1] & 0x20)
        searching2 = bool(data[2] & 0x80)
        print(time.ctime(), binascii.hexlify(data),
              byte, data[0], data[1], data[2], data[3], data[4],
              data[5], data[6], data[7])
        print("    ", signalStrength, pulse, SpO2);
      if time.time() - prevtime > 1.0:
        prevtime = time.time()
        result = '%d pulse %d SpO2 %d # %s' % (int(prevtime + 0.5), pulse,
                                               SpO2, time.ctime(prevtime))
        if self.verbose:
          print(result)
        self.queue.put(result)

class SDI12(object):
  def __init__(self, path, queue, verbose=False):
    self.path = path
    self.queue = queue
    self.verbose = verbose

  def read(self):
    s = serial.Serial(self.path, 9600)
    s.bytesize = serial.EIGHTBITS
    s.parity = serial.PARITY_NONE
    s.stopbits = serial.STOPBITS_TWO
    s.timeout = 1
    s.xonoff = False
    s.rtscts = False
    s.dsrdrt = False
    if self.verbose:
      print("identify")
    s.write(b'?!')
    while True:
      if self.verbose:
        print("measure")
      s.write(b'zM8!')
      if self.verbose:
        print("get values")
      s.write(b'zD0!')
      data = ''
      prevtime = 0
      while True:
        byte = s.read(1)
        if len(byte) != 1:
          break
        data += chr(ord(byte))
        if byte != b'\x0a':
          continue
        if self.verbose:
          print('data', data)
        data = "".join(line.strip() for line in data)
        value = re.sub(r'.*z\+', '', data)
        value = re.sub(r'\+.*$', '', value)
        if value[0] == 'z':
          continue
        if self.verbose:
          print('value', value)
        if time.time() - prevtime > 1.0:
          prevtime = time.time()
          self.queue.put('%d gsr %s # %s' % (int(prevtime + 0.5), value,
                                               time.ctime(prevtime)))

class UA767PC(object):
  def __init__(self, path, queue, verbose=False):
    self.path = path
    self.queue = queue
    self.verbose = verbose

  def read(self):
    while True:
      self.queue.put(result)
      result = '%d bp start # %s' % (time.time(), time.ctime())
      print("START")
      s = serial.Serial(self.path, 9600)
      s.bytesize = serial.EIGHTBITS
      s.parity = serial.PARITY_NONE
      s.stopbits = serial.STOPBITS_TWO
      s.timeout = 5
      s.xonoff = False
      s.rtscts = False
      s.dsrdrt = False
      if self.verbose:
        print("wakeup");
      s.write(b'\x55')
      if self.verbose:
        print("open port")
      time.sleep(.2)
      s.write(b'\x02\x43\x50\x43\x30\x35\x3b') # Open communication port
      while True:
        time.sleep(.1)
        data = s.read(1)
        if len(data) != 1:
          break
        if self.verbose:
          print('data', binascii.hexlify(data))
      if self.verbose:
        print("start")
      time.sleep(.2)
      s.write(b'\x02CPC40\x3a') # Start blood pressure measuring
      if self.verbose:
        print("first packet")
      while True:
        time.sleep(.1)
        data = s.read(1)
        if len(data) != 1:
          break
        if self.verbose:
          print('data', binascii.hexlify(data))
      if self.verbose:
        print("second packet")
      s.timeout = 120
      count = 0
      data = ''
      while count < 10:
        time.sleep(.1)
        byte = s.read(1)
        if len(byte) != 1:
          break
        if self.verbose:
          print('byte', binascii.hexlify(byte))
        data += chr(ord(byte))
        count += 1
      if len(data) == 0:
        continue
      data = "".join(line.strip() for line in data)
      SminusD = int(data[2:4], 16)
      D = int(data[4:6], 16)
      H = int(data[6:8], 16)
      result = '%d sys %d dia %d hr %d # %s' % (time.time(), SminusD + D,
                                                D, H, time.ctime())
      if self.verbose:
        print(result)
      self.queue.put(result)
      s.timeout = 1
      s.close()
      time.sleep(60)

class Monitor(object):
  def __init__(self, list=False, verbose=False):
    self.list = list
    self.verbose = verbose
    self.usb = USB(self.verbose)
    self.blood_pressure = None
    self.pulse_oximiter = None
    self.galvanic_skin_response = None
    for path, busnum, devnum, vendorid, productid, vendor_name, product_name, \
        devices, serial in sorted(self.usb.usb_devices,
                                  key=lambda x: x[1] * 1000 + x[2]):
      if vendorid == 0x0403 and productid == 0x6001:
        if serial == 'A106DFE3':
          self.galvanic_skin_response = devices
        if serial == 'AL061WYE':
          self.blood_pressure = devices
      if vendorid == 0x10c4 and productid == 0xea60:
        self.pulse_oximiter = devices

    if self.list:
      print('Blood pressure (UA-767PC):', self.blood_pressure)
      print('Pulse oximiter (CMS50D+):', self.pulse_oximiter)
      print('Galvanic skin response (SDI-12):', self.galvanic_skin_response)

  def gather(self, file):
    self.file = file
    self.queue = multiprocessing.Queue()
    jobs = []
    if self.blood_pressure:
      ua = UA767PC(os.path.join('/dev', self.blood_pressure[0]),
                   self.queue, self.verbose)
      p = multiprocessing.Process(target=ua.read)
      jobs.append(p)
    if self.pulse_oximiter:
      cms = CMS50DPlus(os.path.join('/dev', self.pulse_oximiter[0]),
                       self.queue, self.verbose)
      p = multiprocessing.Process(target=cms.read)
      jobs.append(p)
    if self.galvanic_skin_response:
      sdi = SDI12(os.path.join('/dev', self.galvanic_skin_response[0]),
                  self.queue, self.verbose)
      p = multiprocessing.Process(target=sdi.read)
      jobs.append(p)
    for p in jobs:
      p.start()
    fd = open(self.file, 'a+')
    while True:
      msg = self.queue.get()
      print(msg)
      fd.write(msg + '\n')
      fd.flush()

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Meditation Monitor')
  parser.add_argument('--list', '-l', action='store_true', default=False,
                      help='List USB ports and suitable devices')
  parser.add_argument('--verbose', '-v', action='store_true', default=False,
                      help='Verbose')
  parser.add_argument('--file', '-f', type=str, default=None,
                      help='Output file for monitoring')
  args = parser.parse_args()

  if not args.list and not args.file:
    parser.print_help()
    sys.exit(-1)

  usb = USB(args.verbose)
  if args.list:
    usb.list()
  monitor = Monitor(args.list, args.verbose)
  if args.list:
    sys.exit(0)
  monitor.gather(args.file)
  sys.exit(0)
